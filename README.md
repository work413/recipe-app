# Recipe Application

This application is a console based one. It starts in the Start.java file where the txt files are read and lists of objects are created using the data in the files. In the class ConsoleUi.java, the communication to the console in being handles. The user is able to create new ingredients and recipes and enter unique information about each object. The user can also delete the objects that exists or search for existing recipes based on ingredient, label, name or price. When the application is existed, the new information is stored in the txt files.
