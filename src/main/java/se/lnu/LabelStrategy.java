package se.lnu;

import java.util.ArrayList;

/**
 * Searches for recipes by label.
 */
public class LabelStrategy implements StrategyInterface {
  private String name = "Label Strategy";

  /**
     * Returns this strategy's name.
     */
  public String getStrategy() {
    return this.name;
  }

  /**
   * Searches through saved recipes using this strategy.
   */
  public ArrayList<CreateRecipe> findRecipies(String labelSearch, ArrayList<CreateRecipe> recipeObjects) {
    ArrayList<CreateRecipe> matchingRecipes = new ArrayList<CreateRecipe>();

    // Loops through recipe objects.
    for (CreateRecipe recipe : recipeObjects) {
      if (recipe.isLabels()) {
        ArrayList<String> labels = recipe.getLabels();
        // Loops through the recipe's labels.
        for (String label : labels) {
          if (label.trim().toLowerCase().equals(labelSearch)) {
            matchingRecipes.add(recipe);
          }
        }
      }
    }
    return matchingRecipes;
  }
    
}