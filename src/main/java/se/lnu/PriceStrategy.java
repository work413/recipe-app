package se.lnu;

import java.util.ArrayList;

/**
 * Searches for recipes by max price.
 */
public class PriceStrategy implements StrategyInterface {
  private String name = "Price Strategy";

  /**
   * Returns this strategy's name.
   */
  public String getStrategy() {
    return this.name;
  }

  /**
   * Searches through saved recipes using this strategy.
   */
  public ArrayList<CreateRecipe> findRecipies(String pstring, ArrayList<CreateRecipe> recipeObjs) {
    // A list of recipe's that costs no more than price searched.
    ArrayList<CreateRecipe> matchingRecipes = new ArrayList<CreateRecipe>();
    int price;
    try {
      price = Integer.parseInt(pstring);
    } catch (NumberFormatException e) {
      price = 0;
    }
    // Loops through recipe objects.
    for (CreateRecipe recipe : recipeObjs) {
      if (recipe.getPrice() <= price) {
        matchingRecipes.add(recipe);
      }
    }
    return matchingRecipes;
  }
}
