package se.lnu;

import java.util.Scanner;

/**
 * Functionality to display the menu alternatives and state messages to the console.
 */
public class ConsoleUi {
  Scanner scan = new Scanner(System.in, "UTF-8");

  /**
   * Displays the menu.
   */
  public void displayMenu() {
    String fillStar = "******************************************";

    System.out.println("\n" + fillStar + "\n Select one of the following options(1-4)\n" + fillStar);
    System.out.println("\n1. Search" + "\n2. Create" + "\n3. Delete" + "\n4. All Recipes" + "\n5. All Ingredients");
  }

  public void choiceError(int firstNum, int secondNum) {
    System.out.println("Please choose a number from " + firstNum + " to " + secondNum);
  }

  public void notFoundError(String item) {
    System.out.println("\n" + item + " not found in library, please try another one.");
  }

  public void searchError() {
    System.out.println("Nothing was found, try searching for something else.");
  }

  /**
   * Displays search sub menu.
   */
  public void search() {
    System.out.println("\nWhat type of search would you like to do?(1-4)\nType 0 to go back one step");
    System.out.println("\n1. Ingredient" + "\n2. Price" + "\n3. Name" + "\n4. Label");
  }

  /**
   * Displays create sub menu.
   */
  public void create() {
    System.out.println("\nWhat would you like to create?(1-2)\nType 0 to go back one step");
    System.out.println("\n1. New Ingredient" + "\n2. New Recipe");
  }

  /**
   * Displays delete sub menu.
   */
  public void delete() {
    System.out.println("\nWhat would you like to delete?(1-2)\nType 0 to go back one step");
    System.out.println("\n1. Ingredient" + "\n2. Recipe");
  }

  public void getName(String item) {
    System.out.println("\nWhat " + item + " would you like to create?");
  }

  public void getIngredientUnit() {
    System.out.println("\nWhat unit is your ingredient measured in?(1-6)");
    System.out.println("\n1. Teaspoon\n2. Tablespoon\n3. Deciliter\n4. Liter\n5. Gram\n6. Piece");
  }

  public void getIngredientPrice() {
    System.out.println("\nWhat is the price per 1 unit of your ingredient?(skr)");
  }

  public void successfullyAdded(String item) {
    System.out.println("\nYour " + item + " was successfully added to file.");
  }

  public void successfullyRemoved(String item) {
    System.out.println("\n" + item + " was successfully removed from file.");
  }

  public void getRecipePortions() {
    System.out.println("\nFor how many portions is your recipe?");
  }

  public void getInstructionToRecipe() {
    System.out.println("\nAdd an instruction for your recipe.");
  }
 
  public void getIngredientToRecipe() {
    System.out.println("What ingredient would you like to add to your recipe?");
  }

  public void getIngredientVolume(String unit) {
    System.out.println("\nHow much of the ingredient is in your recipe? (" + unit + ")");
  }

  public void getCommentToRecipe() {
    System.out.println("\nType a comment if you want to otherwise type \"x\" to save your recipie.");
  }

  public void notUnique() {
    System.out.println("\nThe ingredient you want to create already exists. Please choose another name.");
  }

  public void addLabel() {
    System.out.println("\nWhat is the label?");
  }

  public void whichDelete(String obj) {
    System.out.println("\nWhich of the " + obj + " would you like to delete?");
  }

  public void searchMessage() {
    System.out.println("Please enter your search.");
  }

  public void specificSearch() {
    System.out.println("\nTo choose a specific recipe, type in the name. Else type \"x\" to exit.");
  }

  public void adjustPortions() {
    System.out.println("\nTo adjust portions for recipe, type the number you want. Else type \"x\" to exit.");
  }

  /**
   * Asks the user a yes or no question and handles the answer.
   */
  public boolean positiveAnswer(String question) {
    // Asks the question until user gives yes or no answer.
    System.out.println(question);
    String answer = scan.next();

    // Case for a positive answer.
    if (answer.equals("y") || answer.equals("Y")) {
      return true;
    // Case for a negative answer.
    } else if (answer.equals("n") || answer.equals("N")) {
      return false;
    } else { // Simple error handling.
      System.out.println("\nPlease choose either y or n."); 
      positiveAnswer(question);
    }
    scan.close();
    return false;
  }

  /**
   * Displays recipe data.
   */
  public void getRecipes(CreateRecipe recipe) {
    System.out.println("\n" + recipe.getName() + "\n" + recipe.getPortions() + " portions");
    // Loops through each ingredient element.
    for (CreateIngredient ingredient : recipe.getIngredients()) {
      String unitString = "";
      String ingredientString = "";
      // Prints the unit in plural if the amount is greater than 1.
      if (ingredient.getAmount() > 1) {
        unitString = ingredient.getUnit() + "s";
        ingredientString = ingredient.getAmount() + " " + unitString + " " + ingredient.getName();
      } else {
        ingredientString = ingredient.getAmount() + " " + ingredient.getUnit() + " " + ingredient.getName();
      }
      System.out.println(ingredientString);
    }
    // Prints each instruction.
    for (String instruction : recipe.getInstructions()) {
      System.out.println(instruction);
    }
    if (recipe.isLabels()) { // Prints a list of labels if the recipe has any labels.
      System.out.println("Labels: ");
      // Starts counting from 1 since the first element is the label declaration.
      for (int i = 1; i < recipe.getLabels().size(); i++) {
        System.out.println(recipe.getLabels().get(i));
      }
    } else {
      System.out.println("No labels");
    }
    System.out.println(recipe.getComment());
    String priceString = recipe.getPrice() + " ";
    System.out.println("Full price: " + priceString.replace(".0 ", "") + "SKR");
  }

  /**
   * Display ingredient data.
   */
  public void getIngredients(CreateIngredient ingredient) {
    System.out.println("\n" + ingredient.getName());
    System.out.println("Unit: " + ingredient.getUnit());
    System.out.println("Pice per unit: " + ingredient.getPrice());
  }
}
