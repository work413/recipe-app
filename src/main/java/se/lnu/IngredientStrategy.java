package se.lnu;

import java.util.ArrayList;

/**
 * Searches for recipes by ingredient.
 */
public class IngredientStrategy implements StrategyInterface {
  private String name = "Ingredient Strategy";

  /**
   * Returns this strategy's name.
   */
  public String getStrategy() {
    return this.name;
  }

  /**
   * Searches through saved recipes using this strategy.
   */
  public ArrayList<CreateRecipe> findRecipies(String ingredientSearch, ArrayList<CreateRecipe> recipeObjects) {
    ArrayList<CreateRecipe> matchingRecipes = new ArrayList<CreateRecipe>();

    // Loops through recipe objects.
    for (CreateRecipe recipe : recipeObjects) {
      ArrayList<CreateIngredient> ingredients = recipe.getIngredients();
      // Loops through the recipe's ingredients.
      for (CreateIngredient ingredient : ingredients) {
        if (ingredient.getName().trim().toLowerCase().equals(ingredientSearch)) { // Adds matching recipes to ArrayList.
          matchingRecipes.add(recipe);
        }
      }
    }
    return matchingRecipes;
  }

}
