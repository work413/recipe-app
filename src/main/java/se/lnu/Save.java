package se.lnu;

import java.util.ArrayList;

/**
 * Adds object to lists that will be saved.  
 */
public class Save {
  /**
   * Adds ingredient object to list that will be saved.
   */
  public ArrayList<CreateIngredient> saveIngredient(CreateIngredient i, ArrayList<CreateIngredient> is) {
    is.add(i);
    return is;
  }

  /**
   * Adds recipe object to list that will be saved.
   */
  public ArrayList<CreateRecipe> saveRecipe(CreateRecipe recipe, ArrayList<CreateRecipe> recipeList) {
    recipeList.add(recipe);
    return recipeList;
  }
}
