package se.lnu;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Write txt file from objects.
 */
public class WriteFiles {
  /**
   * Writes all ingredient objects to a txt file.
   */
  public void writeIngredientFile(ArrayList<CreateIngredient> ingredients) throws IOException {
    FileWriter fileWriter = new FileWriter("ingredients.txt", StandardCharsets.UTF_8);
    PrintWriter printWriter = new PrintWriter(fileWriter);

    // Prints each ingredient and it's data to file.
    for (CreateIngredient i : ingredients) {
      printWriter.println(i.getName() + " : " + i.getUnit() + " : " + i.getPrice());
    }
    printWriter.close();
  }

  /**
   * Writes all recipe objects to a txt file.
   */
  public void writeRecipeFile(ArrayList<CreateRecipe> r, ArrayList<CreateIngredient> i) throws IOException {
    FileWriter fileWriter = new FileWriter("recipes.txt", StandardCharsets.UTF_8);
    PrintWriter pw = new PrintWriter(fileWriter);

    for (CreateRecipe cr : r) {
      StringBuffer buffer = new StringBuffer();
      for (String instruction : cr.getInstructions()) {
        buffer.append(instruction + ", "); 
      }
      String inString = buffer.toString();
      String istring = ingString(cr.getIngredients(), i, cr);
      StringBuffer buffer2 = new StringBuffer();
      buffer2.append(cr.getName().trim() + " : ");
      buffer2.append(cr.getPortions() + " : ");
      buffer2.append(istring + " : " + inString + " : ");
      // Does this if there is any labels.
      if (cr.isLabels()) {
        buffer2.append(cr.getLabels() + " : ");
      } 
      buffer2.append(cr.getComment() + " : " + cr.getPrice());
      pw.println(buffer2.toString());
    }
    pw.close();
  }

  /**
   * Creates a string with ingredient data.
   */
  public String ingString(ArrayList<CreateIngredient> recipeI, ArrayList<CreateIngredient> i, CreateRecipe recipe) {
    StringBuffer buffer = new StringBuffer();
    for (CreateIngredient ingredient : recipeI) {
      if (recipe.isIngredient(ingredient.getName().trim(), i)) {
        buffer.append(ingredient.getAmount() + " " + ingredient.getUnit() + " " + ingredient.getName() + ", ");
      }
    }
    String ingredientString = buffer.toString();
    return ingredientString;
  }
}
