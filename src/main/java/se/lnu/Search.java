package se.lnu;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Sets the search strategy.
 */
public class Search {
  private StrategyInterface strategy;
  private ConsoleUi output = new ConsoleUi();
  private Scanner scan = new Scanner(System.in, "UTF-8");

  /**
   * Sets the strategy.
   */
  public void setStrategy(StrategyInterface search) {
    strategy = search;
  }

  /**
   * Asks user for search phrase.
   */
  public void startSearch(ArrayList<CreateRecipe> allRecipes) {
    output.searchMessage();
    String searchWord = scan.next();
    searchWord += scan.nextLine();
    searchResults(strategy.findRecipies(searchWord.toLowerCase(), allRecipes), allRecipes);
  }

  /**
   * Prints the recipes and calls another method.
   */
  public void searchResults(ArrayList<CreateRecipe> recipeList, ArrayList<CreateRecipe> allRecipes) {
    // Simple error handling if the list is empty.
    if (recipeList.size() < 1) {
      output.searchError();
      startSearch(allRecipes);
    }
    CreateRecipe current = new CreateRecipe();
    // Loops through each recipe object, and print it.
    for (CreateRecipe recipe : recipeList) {
      output.getRecipes(recipe);
      current = recipe;
    }
    // Skips a step if the Name Strategy is used.
    if (strategy.getStrategy().equals("Name Strategy")) {
      handlePortions(current);
    } else { 
      setSpecificSearch(recipeList);
    }
  }

  /**
   * Handles user's choice of portions.
   */
  public void setSpecificSearch(ArrayList<CreateRecipe> recipeList) {
    output.specificSearch();
    String nameSearch = scan.next();
    nameSearch += scan.nextLine();
    // Doesn't call the method if the user typed "x".
    if (!nameSearch.toLowerCase().equals("x")) {
      handleRecipeChoice(recipeList, nameSearch.toLowerCase());
    }
  }

  /**
   * Looks for the requested recipe.
   */
  public void handleRecipeChoice(ArrayList<CreateRecipe> recipeList, String search) {
    CreateRecipe foundRecipe = null;
    for (CreateRecipe recipe : recipeList) {
      if (recipe.getName().trim().toLowerCase().equals(search)) {
        foundRecipe = recipe;
      }
    }
    // Simple error handling.
    if (foundRecipe == null) {
      output.notFoundError("Recipe");
      setSpecificSearch(recipeList);
    } else {
      output.getRecipes(foundRecipe);
      handlePortions(foundRecipe);
    }
  }

  /**
   * Handles the portion difference and calls the method that changes the portions.
   */
  public void handlePortions(CreateRecipe recipe) {
    output.adjustPortions();
    int portions = scan.nextInt();
    int oldPortions = recipe.getPortions();
    float difference;
    Boolean increase = false;
    // Checks for portion increase.
    if (portions > oldPortions) {
      difference = (portions / (float) oldPortions);
      increase = true;
    } else if (oldPortions > portions) {
      difference = (oldPortions / (float) portions);
    } else {
      difference = 0;
    }
    changePortions(recipe, difference, increase, portions);
  }

  /**
   * Creates a new recipe objects and prints it.
   */
  public void changePortions(CreateRecipe recipe, Float difference, Boolean increase, int portions) {
    // Creates a new recipe object from the information of the previous recipe object, but with abjusted portions.
    CreateRecipe recipeCopy = new CreateRecipe();
    recipeCopy.setName(recipe.getName());
    recipeCopy.setPortions(portions);
    recipeCopy.setComment(recipe.getComment());
    for (String instruction : recipe.getInstructions()) {
      recipeCopy.setInstructions(instruction.replace("*", ""));
    }
    if (recipe.isLabels()) {
      for (String label : recipe.getLabels()) {
        recipeCopy.setLabel(label);
      }
    }
    // Loops through the recipe's ingredients and creates a new ingredients for each.
    for (CreateIngredient ingredient : recipe.getIngredients()) {
      CreateIngredient newIngredient = new CreateIngredient();
      newIngredient.setName(ingredient.getName().trim());
      newIngredient.setPrice(ingredient.getPrice());
      newIngredient.setUnit(ingredient.getUnit());
      float oldAmount = ingredient.getAmount();
      float newAmount = 0;
      newAmount = (increase) ? oldAmount * difference : oldAmount / difference;
      newAmount = (ingredient.getUnit().trim().equals("piece")) ? Math.round(newAmount) : newAmount;
      recipeCopy.setIngredient(newIngredient, newAmount);
    }
    // Prints the new recipe object.
    output.getRecipes(recipeCopy);
    scan.close();
  }
}

