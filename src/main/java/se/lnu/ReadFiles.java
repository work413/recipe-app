package se.lnu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Reads txt files and create objects from the file.
 */
public class ReadFiles {
  private File recipeFile = new File("recipes.txt");
  private File ingredientFile = new File("ingredients.txt");
  private ArrayList<String> recipeStrings = new ArrayList<String>();
  private ArrayList<String> ingredientStrings = new ArrayList<String>();
  private ArrayList<CreateRecipe> recipeObjects = new ArrayList<CreateRecipe>();
  private ArrayList<CreateIngredient> ingredientObjects = new ArrayList<CreateIngredient>();

  /**
   * Reads files and creates recipe objects.
   */
  public ArrayList<CreateRecipe> getRecipeObjects(ArrayList<CreateIngredient> ingredientList) throws IOException {
    FileReader reader = new FileReader(recipeFile, StandardCharsets.UTF_8);
    BufferedReader bufferReader = new BufferedReader(reader);
    String currentLine;

    // Reads every line in file.
    while ((currentLine = bufferReader.readLine()) != null) {
      recipeStrings.add(currentLine);
    }

    // Loops through strings of recipe data and creates an object for each recipe.
    for (String recipe : recipeStrings) {
      CreateRecipe recipeObj = new CreateRecipe();
      String[] lineSections = recipe.split(":");
      recipeObj.setName(lineSections[0]);
      setPortions(recipeObj, lineSections[1]);
      String[] ingredientsAll = lineSections[2].split(",");
      // Removes the last element that is empty.
      String[] ingredients = Arrays.copyOf(ingredientsAll, ingredientsAll.length - 1);
      setIngredient(recipeObj, ingredients, ingredientList);
      String[] instructionsAll = lineSections[3].split(",");
      // Removes the last element that is empty.
      String[] instructions = Arrays.copyOf(instructionsAll, instructionsAll.length - 1);
      setInstructions(recipeObj, instructions);
      // Does something is recipe had labels.
      if (lineSections.length > 6) {
        String[] labelsAll = lineSections[4].split(",");
        // Removes the first element that is only descriptive.
        String[] labels = Arrays.copyOfRange(labelsAll, 1, labelsAll.length);
        setLabels(recipeObj, labels);
        recipeObj.setComment(lineSections[5].trim());
      } else {
        recipeObj.setComment(lineSections[4].trim());
      }
      recipeObjects.add(recipeObj);
    }
    bufferReader.close();
    return recipeObjects;
  }

  /**
   * Reads files and create ingredient objects.
   */
  public ArrayList<CreateIngredient> getIngredientObjects() throws IOException {
    FileReader reader = new FileReader(ingredientFile, StandardCharsets.UTF_8);
    BufferedReader bufferReader = new BufferedReader(reader);
    String currentLine;

    // Reads every line in file.
    while ((currentLine = bufferReader.readLine()) != null) {
      ingredientStrings.add(currentLine);
    }

    // Loops through string of ingredient data and creates an object for each ingredient.
    for (String ingredient : ingredientStrings) {
      CreateIngredient ingredientObj = new CreateIngredient();
      String[] lineSections = ingredient.split(":");
      ingredientObj.setName(lineSections[0].trim());
      ingredientObj.setUnit(lineSections[1].trim());
      float price;
      // Converts the data type from string to float.
      try {
        price = Float.parseFloat(lineSections[2].trim());
      } catch (NumberFormatException e) {
        price = 0;
      }
      ingredientObj.setPrice(price);
      ingredientObjects.add(ingredientObj);
    }
    bufferReader.close();
    return ingredientObjects;
  }

  /**
   * Converts the data type from string to int and sets the recipe's portions.
   */
  public void setPortions(CreateRecipe recipe, String portions) {
    int portionNum;
    try {
      portionNum = Integer.parseInt(portions.trim());
    } catch (NumberFormatException e) {
      portionNum = 0;
    }
    recipe.setPortions(portionNum);
  }

  /**
   * Sets the correct ingredients to the recipe object.
   */
  public void setIngredient(CreateRecipe r, String[] i, ArrayList<CreateIngredient> list) {
    for (String ingredientData : i) {
      String ingredientName = "";
      float amount = 0;
      for (int j = 3; j < ingredientData.split(" ").length; j++) {
        ingredientName = ingredientData.split(" ")[3].trim();
        amount = Float.parseFloat(ingredientData.split(" ")[1].trim()); 
      }
      r.isIngredient(ingredientName, list);
      r.setIngredient(r.newIngredient, amount);
    }
  }

  /**
   * Sets the correct instructions to recipe object.
   */
  public void setInstructions(CreateRecipe recipe, String[] instructions) {
    for (String instruction : instructions) {
      String inData = instruction.trim();
      recipe.setInstructions(inData.substring(1));
    }
  }

  /**
   * Sets the correct instructions to recipe object.
   */
  public void setLabels(CreateRecipe recipe, String[] labels) {
    recipe.startLabelArray();
    for (String label : labels) {
      // Removes the last element that is unnecessary.
      label = label.replace("]", "");
      String trimmedLabel = label.trim();
      recipe.setLabel(trimmedLabel);
    }
  }
}
