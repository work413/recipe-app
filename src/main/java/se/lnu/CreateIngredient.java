package se.lnu;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Creates an ingredient object.
 */
public class CreateIngredient {
  private String name;
  private String unit;
  private float price;
  private float amount;
  private ConsoleUi output = new ConsoleUi();
  private Scanner scan = new Scanner(System.in, "UTF-8");

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setUnit(String unit) {
    this.unit = unit;
  }

  public String getUnit() {
    return this.unit;
  }

  public void setPrice(Float price) {
    this.price = price;
  }

  public float getPrice() {
    return this.price;
  }

  public void setAmount(float amount) {
    this.amount = amount;
  }

  public float getAmount() {
    return this.amount;
  }

  /**
   * Converts the unit choice number to a string.
   */
  public String convertUnit(int unitNum) {
    String unit = "";
    switch (unitNum) {
      case 1:
        unit += "teaspoon";
        break;
      case 2:
        unit += "tablespoon";
        break;
      case 3:
        unit += "deciliter";
        break;
      case 4:
        unit += "liter";
        break;
      case 5:
        unit += "gram";
        break;
      case 6:
        unit += "piece";
        break;
      default:
        unit = "Unvalid choice.";
        break;
    }
    return unit;
  }
    
  /**
   * Checks if the requested name already exists.
   */
  public Boolean isUnique(ArrayList<CreateIngredient> ingredientList, String newIngredient) {
    Boolean unique = true;
    
    // Loops through each ingredient object.
    for (CreateIngredient ingredient : ingredientList) {
      if (ingredient.getName().trim().toLowerCase().equals(newIngredient.toLowerCase())) {
        unique = false;
      }
    }
    return unique;
  }

  /**
   * Asks the user for information about this new ingredient and sets it.
   */
  public void createNew(ArrayList<CreateIngredient> ingredients) {
    output.getName("ingredient");
    String name = scan.next();
    name += scan.nextLine();

    // Checks is the requested name already exists.
    if (isUnique(ingredients, name)) {
      this.name = name;
      output.getIngredientUnit();
      int unit;
      unit = scan.nextInt();
      // Simple error handling.
      do {
        if (unit < 1 || unit > 6) {
          output.choiceError(1, 6);
          unit = scan.nextInt();
        }
      } while (unit < 1 || unit > 6);
      // Calls the method that converts the number to the correct string.
      String unitName = convertUnit(unit);
      setUnit(unitName);
      output.getIngredientPrice();
      float price = 0;
      try {
        price = scan.nextFloat();
      } catch (InputMismatchException ime) {
        scan.next();
      }
      setPrice(price); 
    } else {
      // Do this if the name input is not unique.
      output.notUnique();
      createNew(ingredients);
    }
  }

}
