package se.lnu;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Removes objects from ArrayLists of recipes and ingredients.
 */
public class Delete {
  private ConsoleUi output = new ConsoleUi();
  private Scanner scan = new Scanner(System.in, "UTF-8");

  /**
   * Asks the user what to delete.
   */
  public int delete(ArrayList<CreateRecipe> recipes, ArrayList<CreateIngredient> ingredients) {
    output.delete();
  
    int choice = scan.nextInt();
    return choice;
  }
  
  /**
   * Removes an ingredient from ArrayList.
   */
  public ArrayList<CreateIngredient> deleteIngredient(ArrayList<CreateIngredient> ingredients) {
    // Loops through each ingredient object.
    for (CreateIngredient ingredient : ingredients) {
      output.getIngredients(ingredient);
    }
    int deleteObjIndex = 0;
    Boolean isInList = false;
    // Asks the user which ingredient to delete.
    output.whichDelete("ingredients");
    String searchString = scan.next();

    for (CreateIngredient ingredient : ingredients) { 
      // Finds the requested object and takes note of it's index.
      if (ingredient.getName().trim().toLowerCase().equals(searchString.toLowerCase())) {
        deleteObjIndex = ingredients.indexOf(ingredient);
        isInList = true;
      }
    }
    if (!isInList) { // Print error message if the requested object was not found.
      output.notFoundError("Ingredient");
      deleteIngredient(ingredients);
    } else {
      // Calls the method that removes the object.
      ingredients.remove(deleteObjIndex);
    }
    return ingredients;
  }
  
  /**
   * Removes a recipe from ArrayList.
   */
  public ArrayList<CreateRecipe> deleteRecipe(ArrayList<CreateRecipe> recipes) {
    // Loops through each recipe object.
    for (CreateRecipe recipe : recipes) {
      output.getRecipes(recipe);
    }
    int deleteObjIndex = 0;
    Boolean isInList = false;
    // Asks the user which recipe to delete.
    output.whichDelete("recipes");
    String searchString = scan.next();

    for (CreateRecipe recipe : recipes) {
      // Finds the requested object and takes note of it's index.
      if (recipe.getName().trim().toLowerCase().equals(searchString.toLowerCase())) { 
        deleteObjIndex = recipes.indexOf(recipe);
        isInList = true;
      }
    }
    if (!isInList) { // Print error message if the requested object was not found.
      output.notFoundError("Recipe");
      deleteRecipe(recipes);
    } else {
      // Calls the method that removes the object.
      recipes.remove(deleteObjIndex);
    }
    return recipes;
  }
  
}
