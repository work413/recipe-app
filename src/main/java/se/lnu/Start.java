package se.lnu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * The start of the application.
 */
public class Start {
  private Scanner scan = new Scanner(System.in, "UTF-8");
  private ConsoleUi output = new ConsoleUi();
  private ArrayList<CreateIngredient> saveIngredients = new ArrayList<CreateIngredient>();
  private ArrayList<CreateRecipe> saveRecipes = new ArrayList<CreateRecipe>();

  /**
   * Handles search options.
   */
  public void search(ArrayList<CreateIngredient> i, ArrayList<CreateRecipe> r) {
    output.search();

    int choice = scan.nextInt();

    if (choice == 0) {
      toMenu(i, r);
    } else if (choice > 4 || choice < 1) {
      // Error handling.
      output.choiceError(1, 4);
      search(i, r);
    } else {
      setSearch(choice, r);
    }
  }

  /**
   * Handles create options.
   */
  public void create(ArrayList<CreateIngredient> i, ArrayList<CreateRecipe> r) {
    output.create();
    int choice = scan.nextInt();
    if (choice == 0) {
      toMenu(i, r);
    } else if (choice > 2 || choice < 1) {
      // Error handling.
      output.choiceError(1, 2);
      create(i, r);
    } else {
      if (choice == 1) {
        createIngredient(i);
      } else { 
        createRecipe(r, i);
      }
    }
  }

  /**
   * Creates an ingredient from user requests.
   */
  public void createIngredient(ArrayList<CreateIngredient> ingredients) {
    CreateIngredient newI = new CreateIngredient();
    Save save = new Save();
    newI.createNew(ingredients);
    saveIngredients = save.saveIngredient(newI, ingredients);
  }

  /**
   * Creates a recipe from user requests.
   */
  public void createRecipe(ArrayList<CreateRecipe> recipes, ArrayList<CreateIngredient> ingredients) {
    CreateRecipe newR = new CreateRecipe();
    Save save = new Save();
    newR.createNew(recipes, ingredients);
    saveRecipes = save.saveRecipe(newR, recipes);
  }

  /**
   * Calls the method that deletes an ingredient from list.
   * Returns the new list.
   */
  public ArrayList<CreateIngredient> deleteI(Delete deleteD, ArrayList<CreateIngredient> ingredients) {
    ArrayList<CreateIngredient> newIngredientList = deleteD.deleteIngredient(ingredients);
    return newIngredientList;
  }

  /**
   * Calls the method that deletes a recipe from list.
   * Returns the new list.
   */
  public ArrayList<CreateRecipe> deleteR(Delete deleteData, ArrayList<CreateRecipe> recipes) {
    ArrayList<CreateRecipe> newRecipeList = deleteData.deleteRecipe(recipes);
    return newRecipeList;
  }

  /**
   * Decides what type of object to delete.
   */
  public void delete(ArrayList<CreateIngredient> i, ArrayList<CreateRecipe> r) {
    Delete deleteData = new Delete();
    int choice = deleteData.delete(r, i);
    if (choice == 0) {
      toMenu(i, r);
    } else if (choice == 1) {
      // Calls the method for deleting an ingredient.
      saveIngredients = deleteI(deleteData, i);
    } else if (choice == 2) {
      // Calls the method for deleting a recipe.
      saveRecipes = deleteR(deleteData, r);
    } else {
      // Simple error handling.
      output.choiceError(1, 2);
      delete(i, r);
    }
  }

  /**
   * Calls the Search class and sets the search strategy.
   */
  public void setSearch(int searchChoice, ArrayList<CreateRecipe> recipeObjects) {
    Search userSearch = new Search();
    switch (searchChoice) {
      case 1:
        userSearch.setStrategy(new IngredientStrategy());
        break;
      case 2:
        userSearch.setStrategy(new PriceStrategy());
        break;
      case 3:
        userSearch.setStrategy(new NameStrategy());
        break;
      case 4:
        userSearch.setStrategy(new LabelStrategy());
        break;
      default:
        userSearch.setStrategy(new NameStrategy());
        break;
    }
    userSearch.startSearch(recipeObjects);
  }

  /**
   * Calls the method that displays the menu options.
   * Handles the user's choice from menu.
   */
  public void toMenu(ArrayList<CreateIngredient> i, ArrayList<CreateRecipe> r) {
    output.displayMenu();

    int firstAlt = scan.nextInt();

    if (firstAlt > 5 || firstAlt < 1) {
      // Simple error handling.
      output.choiceError(1, 5);
    } else {
      switch (firstAlt) {
        case 1:
          search(i, r);
          break;
        case 2:
          create(i, r);
          break;
        case 3:
          delete(i, r);
          break;
        case 4:
          for (CreateRecipe recipe : r) {
            output.getRecipes(recipe);
          }
          break;
        case 5:
          for (CreateIngredient ingredient : i) {
            output.getIngredients(ingredient);
          }
          break;    
        default:
          toMenu(i, r);
          break;
      }
    }
  }

  /**
   * Reads files and creates objects from them.
   * Calls the method that displays the menu.
   * Finally calls the method that write files.
   */
  public void start() throws IOException {
    ReadFiles files = new ReadFiles();
    ArrayList<CreateIngredient> ingredientObjects = files.getIngredientObjects();
    ArrayList<CreateRecipe> recipeObjects = files.getRecipeObjects(ingredientObjects);

    // Displays the menu.
    toMenu(ingredientObjects, recipeObjects);
    
    // Saves old data if nothing has been changed.
    if (saveIngredients.size() < 1) {
      saveIngredients = ingredientObjects;
    }
    if (saveRecipes.size() < 1) {
      saveRecipes = recipeObjects;
    }
    WriteFiles writeFiles = new WriteFiles();
    writeFiles.writeIngredientFile(saveIngredients);
    writeFiles.writeRecipeFile(saveRecipes, saveIngredients);
    scan.close();
  }

  /**
   * The starting point of the application.
   */
  public static void main(String[] args) throws IOException {
    Start current = new Start();
    current.start();
  }
}
