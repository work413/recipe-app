package se.lnu;

import java.util.ArrayList;

/**
 * The Strategy Interface.
 */
public interface StrategyInterface {
  /**
   * Returns the strategy name.
   */
  public String getStrategy();

  /**
   * Searches recipies.
   */
  public ArrayList<CreateRecipe> findRecipies(String word, ArrayList<CreateRecipe> recipeObjects);
}
