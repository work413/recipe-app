package se.lnu;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Creates a recipe object.
 */
public class CreateRecipe {
  private String name;
  private int portions;
  private ArrayList<String> instructions = new ArrayList<>();
  private ArrayList<CreateIngredient> ingredients = new ArrayList<>();
  private String comment;
  private ArrayList<String> labels = new ArrayList<>();
  public CreateIngredient newIngredient;
  private ConsoleUi output = new ConsoleUi();
  private Scanner scan = new Scanner(System.in, "UTF-8");

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  public void setPortions(int amount) {
    this.portions = amount;
  }

  public int getPortions() {
    return this.portions;
  }

  /**
   * Adds and returns the full price of the recipe.
   */
  public float getPrice() {
    float fullPrice = 0;
    // Loops through each ingredient object and adds it's price to the total.
    for (CreateIngredient ingredient : ingredients) {
      fullPrice += ingredient.getPrice() * ingredient.getAmount();
    }
    return fullPrice;
  }

  public void setComment(String comment) {
    // Sets the comment to "No comment" if user typed "x".
    this.comment = (comment.equals("x")) ? "No comment" : comment;
  }

  public String getComment() {
    return this.comment;
  }

  public void setInstructions(String instruction) {
    this.instructions.add("*" + instruction + " ");
  }

  public ArrayList<String> getInstructions() {
    return instructions;
  }

  /**
   * Adds ingredient object to list.
   */
  public void setIngredient(CreateIngredient ingredient, float amount) {
    // First sets the amount for this specific ingredient object.
    ingredient.setAmount(amount);
    this.ingredients.add(ingredient);
  }

  /**
   * Checks if the ingredient requested is listed in ArrayList of existing objects.
   */
  public Boolean isIngredient(String ingredientSearch, ArrayList<CreateIngredient> ingredients) {
    Boolean exists = false;
    // Loops through each ingredient object.
    for (CreateIngredient ingredient : ingredients) {
      // Does this if ingredient is found in list.
      if (ingredient.getName().trim().toLowerCase().equals(ingredientSearch.toLowerCase())) {
        this.newIngredient = ingredient;
        exists = true;
      }
    }
    return exists;
  }

  public CreateIngredient getIngredient() {
    return this.newIngredient;
  }

  public ArrayList<CreateIngredient> getIngredients() {
    return this.ingredients;
  }

  /**
   * Call this method if you want to start an ArrayList of labels.
   */
  public void startLabelArray() {
    this.labels.add("labels; ");
  }

  public void setLabel(String label) {
    this.labels.add(label + " ");
  }

  /**
   * Checks if this object has any labels.
   */
  public Boolean isLabels() {
    Boolean value = (labels.size() > 1) ? true : false;
    return value;
  }

  public ArrayList<String> getLabels() {
    return labels;
  }

  /**
   * Asks the user for information about this new recipe and sets it.
   */
  public void createNew(ArrayList<CreateRecipe> recipes, ArrayList<CreateIngredient> ingredients) {
    output.getName("recipe");
    String name = scan.next();
    name += scan.nextLine();
    this.name = name;
    output.getRecipePortions();
    int portions = scan.nextInt();
    this.portions = portions;
    addIngredientToRecipe(ingredients);
    addInstructionToRecipe();
    String question = "Would you like to add label/s to your recipe?(y/n)";
    if (output.positiveAnswer(question)) {
      startLabelArray();
      addLabel(name, portions);
    }
    output.getCommentToRecipe();
    String comment = scan.next();
    comment += scan.nextLine();
    setComment(comment);
    scan.close();
  }

  /**
   * Asks the user for ingredient to add, checks for the ingredient in the list and finally sets the ingredient.
   */
  private void addIngredientToRecipe(ArrayList<CreateIngredient> ingredients) {
    Boolean isIngredient = false;
    Boolean addIngredient = false;

    do {
      output.getIngredientToRecipe();
      String ingredient = scan.next();
      // Checks if requested ingredient exists in list.
      if (isIngredient(ingredient, ingredients)) {
        isIngredient = true;
        CreateIngredient current = newIngredient;
        output.getIngredientVolume(current.getUnit());
        float amount = 0;
        try {
          amount = scan.nextFloat();
        } catch (InputMismatchException ime) {
          scan.next();
        }
        setIngredient(current, amount);
        String question = ("\nWould you like to add another ingredient to your recipe?(y/n)");
        addIngredient = output.positiveAnswer(question);
      } else {
        // Error handling.
        output.notFoundError("Ingredient");
      }
    } while (isIngredient.equals(false) || addIngredient.equals(true));
  }

  /**
   * Asks the user to add an instruction to the recipe and calls appropriate method.
   */
  public void addInstructionToRecipe() {
    output.getInstructionToRecipe();
    String instruction = scan.next();
    instruction += scan.nextLine();
    setInstructions(instruction);
    String question = "\nWould you like to add another instruction?(y/n)";
    if (output.positiveAnswer(question)) {
      addInstructionToRecipe();
    }
  }

  /**
   * Asks the user to add a label to recipe and calls appropriate method. 
   */
  public void addLabel(String name, int portions) {
    output.addLabel();
    String label = scan.next();
    setLabel(label);
    String q = ("Would you like to add another label?(y/n)");
    if (output.positiveAnswer(q)) {
      addLabel(name, portions);
    }
  }


}

