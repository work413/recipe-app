package se.lnu;

import java.util.ArrayList;

/**
 * Searches for recipes by recipie name.
 */
public class NameStrategy implements StrategyInterface {
  private String name = "Name Strategy";

  /**
   * Returns this strategy's name.
   */
  public String getStrategy() {
    return this.name;
  }

  /**
   * Searches through saved recipes using this strategy.
   */
  public ArrayList<CreateRecipe> findRecipies(String nameSearch, ArrayList<CreateRecipe> recipeObjects) {
    // A list of recipe's that match the name searched.
    ArrayList<CreateRecipe> matchingRecipes = new ArrayList<CreateRecipe>();

    // Loops through recipe objects.
    for (CreateRecipe recipe : recipeObjects) {
      if (recipe.getName().toLowerCase().trim().equals(nameSearch)) {
        matchingRecipes.add(recipe);
      }
    }
    return matchingRecipes;
  }
    
}
