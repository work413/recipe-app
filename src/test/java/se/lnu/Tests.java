package se.lnu;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;

public class Tests {

    // Tests if operation converts number to correct unit string.
    @Test
    public void unitTest() {
      TestCreateIn newIn = new TestCreateIn();
      newIn.convertUnit(4);
      assertEquals("Number converted to wrong measuring unit.", "liter", newIn.convertUnit(4));        
    }

    // Tests if operation performs search for ingredient in ArrayList correctly.
    @Test
    public void isIngredientTest() throws IOException {
        TestRead readFiles = new TestRead();
        TestCreateRe newRe = new TestCreateRe();
        Boolean result = newRe.isIngredient("DummyIngredient", readFiles.getIngredientObjects());
        assertEquals("Searching for spcific ingredient returned wrong value.", false, result);
    }
}

class TestRead extends ReadFiles {
    
}

class TestCreateIn extends CreateIngredient {
    @Override
    public String convertUnit(int unitNum) {
        return super.convertUnit(unitNum);
    }
}

class TestCreateRe extends CreateRecipe {
    @Override
    public Boolean isIngredient(String ingredientSearch, ArrayList<CreateIngredient> ingredients) {
        return super.isIngredient(ingredientSearch, ingredients);
    }
}
