flour : deciliter : 1.0
water : deciliter : 0.0
sugar : deciliter : 2.0
yeast : gram : 0.2
salt : teaspoon : 0.1
oil : deciliter : 1.0
butter : gram : 0.05
milk : deciliter : 1.5
cinnamon : teaspoon : 0.5
egg : piece : 2.0
powdered sugar : tablespoon : 0.8
